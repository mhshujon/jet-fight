package fxml_files_N_classes;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

import java.util.HashMap;

/**
 * Created by MONIR on 12-Aug-16.
 */
public class ScreensController extends StackPane {

    private HashMap <String, Node> screens = new HashMap<>();       //Holds the screens to be displayed.

    public ScreensController() {

        super();
    }

    public void addScreen(String name, Node screen) {           //add the screens to the collection.

        screens.put(name, screen);
    }

//    public Node getScreen(String name) {            //returns the Node with its appropriate name.
//
//        return screens.get(name);
//    }


    //loads the fxml file, add the screen to the screens collection and
    //finally injects the screenPane to the controller.
    public boolean loadScreen(String name, String resource) {

        try {
            FXMLLoader myLoader = new FXMLLoader(getClass().getResource(resource));
            Parent loadScreen = (Parent) myLoader.load();
            ControlledScreen myScreenController = ((ControlledScreen) myLoader.getController());
            myScreenController.setScreenParent(this);
            addScreen(name, loadScreen);
            return true;
        } catch (Exception e) {
            System.out.println("From ScreensController Class: "+e.getMessage());
            return false;
        }
    }


    //This method tries to display the screen with a predefined name.
    //First it makes sure the screen has been already loaded, then if there is more than
    //one screen, the new screen is been added second and then the current screen is removed.
    //If there isn't any screen being displayed, the new screen just added to the root.
    public boolean setScreen(final String name) {

        if (screens.get(name) != null) {//means screen loaded already

            final DoubleProperty opacity = opacityProperty();

            if (!getChildren().isEmpty()) {

                Timeline fade = new Timeline(
                        new KeyFrame(Duration.ZERO, new KeyValue(opacity, 1.0)),
                        new KeyFrame(new Duration(10), new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {

                                getChildren().remove(0);                                //removes the displayed screen.
                                getChildren().add(0, screens.get(name));               //add the screen.
                                Timeline fadeIn = new Timeline(
                                        new KeyFrame(Duration.ZERO, new KeyValue(opacity, 0.0)),
                                        new KeyFrame(new Duration(10), new KeyValue(opacity, 1.0)));
                                fadeIn.play();
                            }
                        }, new KeyValue(opacity, 0.0)));

                fade.play();
            } else  {

                setOpacity(0.0);
                getChildren().add(screens.get(name));                   //no one else been displayed, then just show.
                Timeline fadeIn = new Timeline(
                        new KeyFrame(Duration.ZERO, new KeyValue(opacity, 0.0)),
                        new KeyFrame(new Duration(10), new KeyValue(opacity, 1.0)));
                fadeIn.play();
            }
            return true;
        } else {

            System.out.println("From ScreensController Class: Screen hasn't been loaded.\n");
            return false;
        }
    }

//    public boolean unLoadScreen (String name) {
//
//        if ( screens.remove(name) == null) {
//
//            System.out.println("From ScreensController Class: Screen didn't exist");
//            return false;
//        } else {
//
//            return true;
//        }
//    }
}
