package fxml_files_N_classes;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import screen_controller_class.MenuScreenController;

/**
 * Created by MONIR on 12-Aug-16.
 */
public class ScreensFrameWork extends Application {

    public static String welcomePageID = "welcomePage";
    public static String welcomePageFile = "welcome_page.fxml";
    public static String collusionPageID = "collusion";
    public static String collusionPageFile = "collusion_page.fxml";
    public static String menuPageID = "menuPage";
    public static String menuPageFile = "menu_page.fxml";
    public static String highScorePageID = "highScorePage";
    public static String highScorePageFile = "highScore_page.fxml";
    public static String gamePageID = "gamePage";
    public static String gamePageFile = "game_page.fxml";
    public static String nameEntryPageID = "nameEntryPage";
    public static String nameEntryPageFile = "nameEntry_page.fxml";
    public static String instructionPageID = "instructionPage";
    public static String instructionPageFile = "instruction_page.fxml";
    public static String creditPageID = "creditPage";
    public static String creditPageFile = "credit_page.fxml";


    //private Thread thread = new Thread(this);

    public Stage stage;


    @Override
    public void start(Stage primaryStage) throws Exception {

        ScreensController mainContainer = new ScreensController();

        mainContainer.loadScreen(ScreensFrameWork.menuPageID, ScreensFrameWork.menuPageFile);
        mainContainer.loadScreen(ScreensFrameWork.highScorePageID, ScreensFrameWork.highScorePageFile);
        mainContainer.loadScreen(ScreensFrameWork.gamePageID, ScreensFrameWork.gamePageFile);
        mainContainer.loadScreen(ScreensFrameWork.welcomePageID, ScreensFrameWork.welcomePageFile);
        mainContainer.loadScreen(ScreensFrameWork.collusionPageID, ScreensFrameWork.collusionPageFile);
        mainContainer.loadScreen(ScreensFrameWork.nameEntryPageID, ScreensFrameWork.nameEntryPageFile);
        mainContainer.loadScreen(ScreensFrameWork.instructionPageID, ScreensFrameWork.instructionPageFile);
        mainContainer.loadScreen(ScreensFrameWork.creditPageID, ScreensFrameWork.creditPageFile);

        mainContainer.setScreen(ScreensFrameWork.welcomePageID);       //displaying the first starting screen.
        stage = primaryStage;
        stage.setTitle("Jet Fighter");
        //stage.initStyle(StageStyle.TRANSPARENT);
        Group root = new Group();
        root.getChildren().addAll(mainContainer);
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
        //stage.close();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
