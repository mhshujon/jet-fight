package fxml_files_N_classes;

import javafx.stage.Stage;

/**
 * Created by MONIR on 29-Aug-16.
 */
public class ExitProgram extends ScreensFrameWork {

    private boolean answer;

    public ExitProgram(boolean answer) {
        this.answer = answer;
    }

    public void exit() {

        if (answer == true) {

            stage.close();
        }
        System.out.println("Answer: "+answer);
    }
}
