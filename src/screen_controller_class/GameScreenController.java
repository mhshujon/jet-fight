package screen_controller_class;

import fxml_files_N_classes.ControlledScreen;
import fxml_files_N_classes.ScreensController;
import fxml_files_N_classes.ScreensFrameWork;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.geometry.Bounds;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import other_Codes.GameScreenExtended;
//import other_Codes.GameScreenExtended;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by MONIR on 12-Aug-16.
 */
public class GameScreenController implements ControlledScreen {

    ScreensController myController;
//    NameEntryScreenController objNameEntry;
//    Thread thread = objNameEntry.gameThread;

//    private FileReader fr = new FileReader("src\\files.ControllerValue.txt");
//    private BufferedReader fileRead = new BufferedReader(fr);
//    private String choiceBoxValue = null;

    private boolean threadStart = false;
    private boolean assistThread = true;
//    public boolean stotThread = false;

    private Bounds playerRect;
    private Bounds bulletRect;
    private Bounds enemyRect1;
    private Bounds enemyRect2;
    private Bounds enemyRect3;
    private Bounds enemyRect4;
    private Bounds enemyRect5;
    private Bounds enemyRect6;
    private Bounds enemyRect7;
    private boolean collusion;

    private long startTime;
    private long curTime;

    @FXML
    AnchorPane anchorPane;
    @FXML
    private ImageView playerJet;
    @FXML
    private ImageView bullet;
    @FXML
    private ImageView enemyJet1;
    @FXML
    private ImageView enemyJet2;
    @FXML
    private ImageView enemyJet3;
    @FXML
    private ImageView enemyJet4;
    @FXML
    private ImageView enemyJet5;
    @FXML
    private ImageView enemyJet6;
    @FXML
    private ImageView enemyJet7;
    @FXML
    private ImageView enemyJet8;
    @FXML
    private ImageView enemyJet9;



//    public GameScreenController() throws IOException {}


//    @Override
//    public void initialize(URL location, ResourceBundle resources) {
//
//        String tempString = null;
//        try {
//            tempString = fileRead.readLine();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        if (tempString == null){}
//        else {
//
//            StringTokenizer token = new StringTokenizer(tempString);
//            while(token.hasMoreTokens()) {
//
//                choiceBoxValue = token.nextToken();
//            }
//        }
//        System.out.println("ArrowKeys: "+choiceBoxValue);
//    }

    @Override
    public void setScreenParent(ScreensController screenPage) {

        myController = screenPage;
    }


    @FXML
    private void moveImage(KeyEvent event) {

        double X = playerJet.getX();
        double Y = playerJet.getY();
        double bulletX = bullet.getX();
//        double bulletY = bullet.getY();

//        if (choiceBoxValue.equals("ArrowKeys")) {

            switch (event.getCode()) {

                case ESCAPE:
                    myController.setScreen(ScreensFrameWork.menuPageID);
                    setImageInitialPosition();
                    //stotThread = true;
                    break;

                case LEFT:
                case NUMPAD1:
                    if (X < 10) break;

                    playerJet.setX(playerJet.getX()-10);
                    System.out.println("X: "+playerJet.getX()+"\t");
                    bullet.setX(bullet.getX()-10);
                    break;

                case RIGHT:
                case NUMPAD3:
                    if (X > 1120) break;

                    playerJet.setX(playerJet.getX()+10);
                    System.out.println("X: "+playerJet.getX()+"\t");
                    bullet.setX(bullet.getX()+10);
                    break;

                case UP:
                case NUMPAD5:
                    if (Y < 10) break;

                    playerJet.setY(playerJet.getY()-10);
                    System.out.println("Y: "+playerJet.getY()+"\t");
                    bullet.setY(bullet.getY()-10);
                    break;

                case DOWN:
                case NUMPAD2:
                    if (Y > 640) break;

                    playerJet.setY(playerJet.getY()+10);
                    System.out.println("Y: "+playerJet.getY()+"\t");
                    bullet.setY(bullet.getY()+10);
                    System.out.println("Assist Thread: "+assistThread);
                    if (assistThread == true) {
                        System.out.println("Happening. . .");
                        threadStart = true;
                        assistThread = false;

                        if (threadStart == true)
                            new Thread(new GameScreenExtended(enemyJet1, enemyJet2, enemyJet3, enemyJet4, enemyJet5,
                                    enemyJet6, enemyJet7, enemyJet8, enemyJet9));
                    }
                    System.out.println("Assist Thread: "+assistThread);
                    break;

                case SPACE:
                    //if (bulletX > 1490) break;
                    startTime = System.currentTimeMillis();
                    curTime = startTime;

                    while (bullet.getX() < 1120) {
                        //try {
                            bullet.setX(bullet.getX()+5);
                            System.out.println("X: "+bullet.getX());

                            //Thread.sleep(1);
                        //} catch (InterruptedException e){}

                    }
                    //bullet.setX(77);
                    System.out.println("Shooting..."+bullet.getX());
                    break;

//                case ENTER:
//
//                    break;
//                case SHIFT:
//                    enemyJet1.setVisible(true);
//                    break;
            }
//        }
//        else if (choiceBoxValue.equals("NumPad")) {
//
//            switch (event.getCode()) {
//
//                case ESCAPE:
//                    myController.setScreen(ScreensFrameWork.menuPageID);
//                    setImageInitialPosition();
//                    break;
//
//                case NUMPAD1:
//                    if (X < 10)
//                        break;
//
//                    playerJet.setX(playerJet.getX()-10);
//                    System.out.println("X: "+playerJet.getX()+"\t");
//                    break;
//
//                case NUMPAD3:
//                    if (X > 1340)
//                        break;
//
//                    playerJet.setX(playerJet.getX()+10);
//                    System.out.println("X: "+playerJet.getX()+"\t");
//                    break;
//
//                case NUMPAD5:
//                    if (Y < 10)
//                        break;
//
//                    playerJet.setY(playerJet.getY()-10);
//                    System.out.println("Y: "+playerJet.getY()+"\t");
//                    break;
//
//                case NUMPAD2:
//                    if (Y > 690)
//                        break;
//
//                    playerJet.setY(playerJet.getY()+10);
//                    System.out.println("Y: "+playerJet.getY()+"\t");
//                    break;
//            }
//        }
//        isCollusion();

    }

    public void setImageInitialPosition() {

        enemyJet1.setX(100);
        enemyJet2.setY(500);
    }

    public boolean isCollusion () {

        playerRect = playerJet.getBoundsInParent();
        bulletRect = bullet.getBoundsInParent();
        enemyRect1 = enemyJet1.getBoundsInParent();
        enemyRect2 = enemyJet2.getBoundsInParent();
        enemyRect3 = enemyJet3.getBoundsInParent();
        enemyRect4 = enemyJet4.getBoundsInParent();
        enemyRect5 = enemyJet5.getBoundsInParent();
        enemyRect6 = enemyJet6.getBoundsInParent();
        enemyRect7 = enemyJet7.getBoundsInParent();
        if ( bulletRect.intersects(enemyRect1) || bulletRect.intersects(enemyRect2)
                || bulletRect.intersects(enemyRect3) || bulletRect.intersects(enemyRect4)
                || bulletRect.intersects(enemyRect5) || bulletRect.intersects(enemyRect6)
                || bulletRect.intersects(enemyRect7) || enemyRect1.intersects(bulletRect)
                || enemyRect2.intersects(bulletRect) || enemyRect3.intersects(bulletRect)
                || enemyRect4.intersects(bulletRect) || enemyRect5.intersects(bulletRect)
                || enemyRect6.intersects(bulletRect) || enemyRect7.intersects(bulletRect) )
        {
            System.out.println("Collusion Detected!!");
            myController.setScreen(ScreensFrameWork.collusionPageID);
            myController.setScreen(ScreensFrameWork.gamePageID);
            collusion = true;
            return collusion;
        }
        else
            return collusion;
    }
}
