package screen_controller_class;

import fxml_files_N_classes.ControlledScreen;
import fxml_files_N_classes.ScreensController;
import fxml_files_N_classes.ScreensFrameWork;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import other_Codes.WriteName;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by MONIR on 16-Aug-16.
 */
public class NameEntryScreenController implements Initializable, ControlledScreen {

    ScreensController myController;


    @FXML
    TextField nameField;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @Override
    public void setScreenParent(ScreensController screenPage) {

        myController = screenPage;
    }

    @FXML
    private void gameScreenButtonKey(KeyEvent event) throws IOException {

        switch (event.getCode()) {

            case ENTER:
                System.out.println(event.getCode());
                myController.setScreen(ScreensFrameWork.gamePageID);
                new WriteName(nameField.getText()).write();
                nameField.setText("");
                break;
        }
    }

    @FXML
    private void menuPageButtonKey(KeyEvent event) {

        switch (event.getCode()) {

            case ESCAPE:
                System.out.println(event.getCode());
                myController.setScreen(ScreensFrameWork.menuPageID);
                break;
        }
    }
}
