package screen_controller_class;

import fxml_files_N_classes.ControlledScreen;
import fxml_files_N_classes.ScreensController;
import fxml_files_N_classes.ScreensFrameWork;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.KeyEvent;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by MONIR on 13-Aug-16.
 */
public class WelcomeScreenController implements ControlledScreen, Initializable {

    ScreensController myController;

    @FXML
    MediaView mediaView1;

    private Media media1;
    private MediaPlayer mediaPlayer1;

    @FXML
    MediaView mediaView2;

    private Media media2;
    public MediaPlayer mediaPlayer2;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        String audioPath1 = new File("src/audio/Welcome.mp3").getAbsolutePath();

        media1 = new Media(new File(audioPath1).toURI().toString());
        mediaPlayer1 = new MediaPlayer(media1);
        mediaView1.setMediaPlayer(mediaPlayer1);
        mediaPlayer1.setCycleCount(mediaPlayer1.INDEFINITE);
        mediaPlayer1.play();

        String audioPath2 = new File("src/audio/Fantasy_Game_Background_Looping.mp3").getAbsolutePath();

        media2 = new Media(new File(audioPath2).toURI().toString());
        mediaPlayer2 = new MediaPlayer(media2);
        mediaView2.setMediaPlayer(mediaPlayer2);
        mediaPlayer2.setCycleCount(mediaPlayer2.INDEFINITE);
    }

    @Override
    public void setScreenParent(ScreensController screenPage) {

        myController = screenPage;
    }

    @FXML
    private void goToMenuPage(KeyEvent event) {

        System.out.println(event.getCode());
        myController.setScreen(ScreensFrameWork.menuPageID);
        mediaPlayer1.stop();
        mediaPlayer2.play();
    }
}
