package screen_controller_class;

import fxml_files_N_classes.ControlledScreen;
import fxml_files_N_classes.ScreensController;
import fxml_files_N_classes.ScreensFrameWork;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.KeyEvent;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by MONIR on 16-Aug-16.
 */
public class CreditScreenController implements ControlledScreen {

    ScreensController myController;


    @Override
    public void setScreenParent(ScreensController screenPage) {

        myController = screenPage;
    }

    @FXML
    private void menuPageButtonKey(KeyEvent event)
    {
        switch (event.getCode()) {

            case ESCAPE:
                System.out.println(event.getCode());
                myController.setScreen(ScreensFrameWork.menuPageID);
                break;
        }
    }
}
