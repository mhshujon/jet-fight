package screen_controller_class;

import fxml_files_N_classes.ControlledScreen;
import fxml_files_N_classes.ScreensController;
import fxml_files_N_classes.ScreensFrameWork;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by MONIR on 16-Aug-16.
 */
public class HighScoreScreenController implements ControlledScreen {

    ScreensController myController;


    @FXML
    public Label nameArea;
    private String name;
//    @FXML
//    TextArea highScore;

    public HighScoreScreenController(){super();}
    public HighScoreScreenController(String name) {
        this.name = name;
        System.out.println("NAME1: "+name);
//        try {
//            nameArea.setText(name);
//        } catch (Exception e) {
//            System.out.println("Line 37: "+e.getMessage().toString());
//        }
    }

    @Override
    public void setScreenParent(ScreensController screenPage) {

        myController = screenPage;
    }

    @FXML
    private void menuPageButtonKey(KeyEvent event)
    {
        switch (event.getCode()) {

            case ESCAPE:
                System.out.println(event.getCode());
                myController.setScreen(ScreensFrameWork.menuPageID);
                break;
        }
    }
}
