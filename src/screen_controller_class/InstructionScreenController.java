package screen_controller_class;

import com.sun.org.apache.xml.internal.security.Init;
import fxml_files_N_classes.ControlledScreen;
import fxml_files_N_classes.ScreensController;
import fxml_files_N_classes.ScreensFrameWork;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.input.KeyEvent;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by MONIR on 16-Aug-16.
 */
public class InstructionScreenController implements Initializable, ControlledScreen {

    ScreensController myController;

    ObservableList<String> choiceBoxList = FXCollections.observableArrayList("ArrowKeys", "NumPad");

    @FXML
    private ChoiceBox choiceBox;

    public ChoiceBox passChoiceBox;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        choiceBox.setValue("ArrowKeys");
        choiceBox.setItems(choiceBoxList);
        passChoiceBox = choiceBox;
    }

    @Override
    public void setScreenParent(ScreensController screenPage) {

        myController = screenPage;
    }

    @FXML
    private void menuPageButtonKey(KeyEvent event)
    {
        switch (event.getCode()) {

            case ESCAPE:
                System.out.println(event.getCode());
                myController.setScreen(ScreensFrameWork.menuPageID);
                break;
        }
    }
}
