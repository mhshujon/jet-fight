package screen_controller_class;


import fxml_files_N_classes.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.KeyEvent;
import other_Codes.AlertBox;
import other_Codes.ReadName;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by MONIR on 12-Aug-16.
 */
public class MenuScreenController implements ControlledScreen {


    ScreensController myController;

    public boolean answer;


    @Override
    public void setScreenParent(ScreensController screenPage) {

        myController = screenPage;
    }

    @FXML
    private void nameEntryButtonAction(ActionEvent event) {

//        button.removeEventHandler(KeyEvent.KEY_TYPED, null);

        System.out.println("It is it: "+event.getEventType().getName());
        myController.setScreen(ScreensFrameWork.nameEntryPageID);
    }

    @FXML
    private void highScoreButtonAction(ActionEvent event) {

        System.out.println("It is it: "+event.getEventType().getName());
        System.out.println("name entry: "+event.getSource().hashCode());
//        try {
//            new ReadName().read();
//        } catch (IOException e) {
//            System.out.println("Line 50: "+e.getMessage().toString());
//        }

        myController.setScreen(ScreensFrameWork.highScorePageID);
    }

    @FXML
    public void instructionButtonAction(ActionEvent event)
    {
        System.out.println("It is it: "+event.getEventType().getName());
        myController.setScreen(ScreensFrameWork.instructionPageID);
    }

    @FXML
    private void creditButtonAction(ActionEvent event)
    {
        System.out.println("It is it: "+event.getEventType().getName());
        myController.setScreen(ScreensFrameWork.creditPageID);
    }

//----------------------------------------------------------------------------------------------------------------------

    @FXML
    private void nameEntryButtonKey(KeyEvent event)
    {
        switch (event.getCode()) {

            case ENTER:
                System.out.println(event.getCode());
                myController.setScreen(ScreensFrameWork.nameEntryPageID);
                break;
        }
    }

    @FXML
    private void highScoreButtonKey(KeyEvent event)
    {
        switch (event.getCode()) {

            case ENTER:
                System.out.println(event.getCode());
                myController.setScreen(ScreensFrameWork.highScorePageID);
                break;
        }
    }

    @FXML
    private void instructionButtonKey(KeyEvent event)
    {
        {
            switch (event.getCode()) {

                case ENTER:
                    System.out.println(event.getCode());
                    myController.setScreen(ScreensFrameWork.instructionPageID);
                    break;
            }
        }
    }

    @FXML
    private void creditButtonKey(KeyEvent event)
    {
        {
            switch (event.getCode()) {

                case ENTER:
                    System.out.println(event.getCode());
                    myController.setScreen(ScreensFrameWork.creditPageID);
                    break;
            }
        }
    }

    @FXML
    public void exitButtonKey(KeyEvent event) {

        switch (event.getCode()) {

            case ENTER:
                answer = new AlertBox().display("Attention!","Do you really want to exit?");
                System.out.println("Answer: "+answer);
                new ExitProgram(answer).exit();
                break;
        }
    }
}
