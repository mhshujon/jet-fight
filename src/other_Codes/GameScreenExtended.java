package other_Codes;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.image.ImageView;
import screen_controller_class.GameScreenController;

/**
 * Created by MONIR on 25-Aug-16.
 */
public class GameScreenExtended implements Runnable {

    private boolean stopThread = false;

    ImageView enemyJet1;
    ImageView enemyJet2;
    ImageView enemyJet3;
    ImageView enemyJet4;
    ImageView enemyJet5;
    ImageView enemyJet6;
    ImageView enemyJet7;
    ImageView enemyJet8;
    ImageView enemyJet9;

    public GameScreenExtended(ImageView enemyJet1, ImageView enemyJet2, ImageView enemyJet3, ImageView enemyJet4,
                              ImageView enemyJet5, ImageView enemyJet6, ImageView enemyJet7, ImageView enemyJet8, ImageView enemyJet9) {

        this.enemyJet1 = enemyJet1;
        this.enemyJet2 = enemyJet2;
        this.enemyJet3 = enemyJet3;
        this.enemyJet4 = enemyJet4;
        this.enemyJet5 = enemyJet5;
        this.enemyJet6 = enemyJet6;
        this.enemyJet7 = enemyJet7;
        this.enemyJet8 = enemyJet8;
        this.enemyJet9 = enemyJet9;

        new Thread(this).start();
    }

    @Override
    public void run() {

        System.out.println("Thread Has Started!!");

        while (true) {

            try {
                double x1 = enemyJet1.getX();
                enemyJet1.setX(x1-15);
                if (x1<-60)
                    enemyJet1.setX(1500);
                Thread.currentThread().sleep(10);

                double x2 = enemyJet2.getX();
                enemyJet2.setX(x2-15);
                if (x2<-60)
                    enemyJet2.setX(1500);
                Thread.currentThread().sleep(10);

                double x3 = enemyJet3.getX();
                enemyJet3.setX(x3-15);
                if (x3<-60)
                    enemyJet3.setX(1500);
                Thread.currentThread().sleep(10);

                double x4 = enemyJet4.getX();
                enemyJet4.setX(x4-15);
                if (x4<-60)
                    enemyJet4.setX(1500);
                Thread.currentThread().sleep(10);

                double x5 = enemyJet5.getX();
                enemyJet5.setX(x5-15);
                if (x5<-60)
                    enemyJet5.setX(1500);
                Thread.currentThread().sleep(10);

                double x6 = enemyJet6.getX();
                enemyJet6.setX(x6-15);
                if (x6<-60)
                    enemyJet6.setX(1500);
                Thread.currentThread().sleep(10);

                double x7 = enemyJet7.getX();
                enemyJet7.setX(x7-15);
                if (x7<-60)
                    enemyJet7.setX(1500);
                Thread.currentThread().sleep(10);

                double x8 = enemyJet8.getX();
                enemyJet8.setX(x8-15);
                if (x8<-60)
                    enemyJet8.setX(1500);
                Thread.currentThread().sleep(10);

                double x9 = enemyJet9.getX();
                enemyJet9.setX(x9-15);
                if (x9<-60)
                    enemyJet9.setX(1500);
                Thread.currentThread().sleep(5);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
