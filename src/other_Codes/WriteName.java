package other_Codes;

import javafx.scene.control.TextField;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by MONIR on 29-Aug-16.
 */
public class WriteName {

    private String name;

    public WriteName(String name) {
        this.name = name;
    }

    public void write() throws IOException {

        FileWriter fileWriter = new FileWriter("src\\files\\ControllerValue.txt");
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

        System.out.println("Name: "+name);
        try {
            bufferedWriter.write(name);
            bufferedWriter.flush();
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
